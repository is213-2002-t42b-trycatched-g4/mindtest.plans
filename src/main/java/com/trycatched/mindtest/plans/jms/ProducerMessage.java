package com.trycatched.mindtest.plans.jms;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

@Component
public class ProducerMessage {

	@Autowired
	private JmsTemplate jmsTemplate;

	@Value("${jms.cola.envio}")
	private String colaDestiny;

	public void sendMessage(String message) {
		String uuid = UUID.randomUUID().toString();
		jmsTemplate.convertAndSend(colaDestiny, message, m -> {
			m.setJMSCorrelationID(uuid);
			return m;
		});
	}

}
