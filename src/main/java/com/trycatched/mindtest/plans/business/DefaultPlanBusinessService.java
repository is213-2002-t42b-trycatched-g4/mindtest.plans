package com.trycatched.mindtest.plans.business;

import java.io.IOException;

import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.trycatched.mindtest.plans.client.ApiClientAuthorization;
import com.trycatched.mindtest.plans.config.ApiRestConfig;
import com.trycatched.mindtest.plans.dto.RequestPlan;
import com.trycatched.mindtest.plans.dto.TrackData;
import com.trycatched.mindtest.plans.entity.Plan;
import com.trycatched.mindtest.plans.jms.ProducerMessage;
import com.trycatched.mindtest.plans.repository.PlanRepository;

import util.constant.ApiError;
import util.dto.ApiException;
import util.dto.ApiResponse;

@Service
public class DefaultPlanBusinessService implements PlanBusinessService {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	private PlanRepository planRepository;

	private ApiRestConfig restConfig;

	private ApiClientAuthorization apiClientAuth;

	private static final String BEARER = "Bearer ";

	private ProducerMessage producerMessage;

	@Autowired
	public DefaultPlanBusinessService(PlanRepository planRepository, ApiClientAuthorization apiClientAuth, ApiRestConfig restConfig, ProducerMessage producerMessage) {
		this.planRepository = planRepository;
		this.apiClientAuth = apiClientAuth;
		this.restConfig = restConfig;
		this.producerMessage = producerMessage;
	}

	@Override
	public ApiResponse purchasePlan(String authorization, RequestPlan requestPlan) throws ApiException {

		Long idUser = requestPlan.getIdUser();
		int idPlan = requestPlan.getIdPlan();

		ApiResponse authResponse = consultAuthorization(authorization);

		if (isUnauthorized(authResponse)) {
			throw ApiException.of(ApiError.USER_UNAUTHORIZAED.getCode(), ApiError.USER_UNAUTHORIZAED.getMessage());
		}

		ApiResponse existResponse = consultUser(idUser);

		if (isNotExistsUser(existResponse)) {
			throw ApiException.of(ApiError.USER_NOT_FOUND.getCode(), ApiError.USER_NOT_FOUND.getMessage());
		}

		Plan plan = planRepository.findById(idPlan)
				.orElseThrow(() -> ApiException.of(ApiError.PLAN_NOT_FOUND.getCode(), ApiError.PLAN_NOT_FOUND.getMessage()));


		String message = buildMessageForTracking(idUser, plan);
		logger.info("Mensaje construido: {}", message);

		sendMessage(message);

		return ApiResponse.of(ApiError.SUCCESS.getCode(), ApiError.SUCCESS.getMessage(), "Mensaje enviado");
	}

	private void sendMessage(String message) {
		producerMessage.sendMessage(message);
	}

	private boolean isNotExistsUser(ApiResponse existResponse) throws ApiException {
		JsonNode root = null;
		String message = existResponse.getMessage();
		try {
			root = new ObjectMapper().readTree(message);
			logger.info("result capturado: {}", root);
		} catch (Exception e) {
			throw ApiException.of(ApiError.NO_APPLICATION_PROCESSED.getCode(), ApiError.NO_APPLICATION_PROCESSED.getMessage(), e.getMessage());
		}
		return !root.path("object").asBoolean();
	}

	private ApiResponse consultUser(Long idUser) throws ApiException {
		CloseableHttpClient httpclient = HttpClients.createDefault();
		ApiResponse authResponse;
		try {
			authResponse = apiClientAuth.existsUser(httpclient, restConfig.getUrlExistsUser(), idUser);
		} catch (IOException e) {
			throw ApiException.of(ApiError.NO_APPLICATION_PROCESSED.getCode(), ApiError.NO_APPLICATION_PROCESSED.getMessage(), e.getMessage());
		}
		return authResponse;
	}

	private String buildMessageForTracking(Long userId, Plan plan) throws ApiException {
		TrackData data = new TrackData(userId, plan);
		ObjectMapper mapper = new ObjectMapper();
		String content = null;
		try {
			content = mapper.writeValueAsString(data);
		} catch (JsonProcessingException e) {
			throw ApiException.of(ApiError.NO_APPLICATION_PROCESSED.getCode(), ApiError.NO_APPLICATION_PROCESSED.getMessage(), e.getMessage());
		}
		return content;
	}

	private ApiResponse consultAuthorization(String authorization) throws ApiException {
		String token = extractToken(authorization);
		logger.info("token: {}", token);

		CloseableHttpClient httpclient = HttpClients.createDefault();
		ApiResponse authResponse;
		try {
			authResponse = apiClientAuth.verifyAuthorization(httpclient, restConfig.getUrlCheckToken(), token);
		} catch (IOException e) {
			throw ApiException.of(ApiError.NO_APPLICATION_PROCESSED.getCode(), ApiError.NO_APPLICATION_PROCESSED.getMessage(), e.getMessage());
		}
		return authResponse;
	}

	private boolean isUnauthorized(ApiResponse authResponse) throws ApiException {
		JsonNode root = null;
		String message = authResponse.getMessage();
		try {
			root = new ObjectMapper().readTree(message);
			logger.info("result capturado: {}", root);
		} catch (Exception e) {
			throw ApiException.of(ApiError.NO_APPLICATION_PROCESSED.getCode(), ApiError.NO_APPLICATION_PROCESSED.getMessage(), e.getMessage());
		}
		return !root.path("object").asBoolean();
	}

	private String extractToken(String authorization) {
		return authorization.substring(BEARER.length(), authorization.length());
	}

}
