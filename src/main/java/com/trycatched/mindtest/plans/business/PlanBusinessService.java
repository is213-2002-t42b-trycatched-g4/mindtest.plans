package com.trycatched.mindtest.plans.business;

import com.trycatched.mindtest.plans.dto.RequestPlan;

import util.dto.ApiException;
import util.dto.ApiResponse;

public interface PlanBusinessService {

	public ApiResponse purchasePlan(String authorization, RequestPlan requestPlan) throws ApiException;

}
