package com.trycatched.mindtest.plans.dto;

import java.io.Serializable;
import java.util.Date;

public class TrackStep implements Serializable {

	private static final long serialVersionUID = -3193822422422655284L;

	private int idStep;
	private String stepDesc;
	private int stepActive;
	private int stepOrder;
	private Date stepCreated;

	public int getIdStep() {
		return idStep;
	}

	public void setIdStep(int idStep) {
		this.idStep = idStep;
	}

	public String getStepDesc() {
		return stepDesc;
	}

	public void setStepDesc(String stepDesc) {
		this.stepDesc = stepDesc;
	}

	public int getStepActive() {
		return stepActive;
	}

	public void setStepActive(int stepActive) {
		this.stepActive = stepActive;
	}

	public int getStepOrder() {
		return stepOrder;
	}

	public void setStepOrder(int stepOrder) {
		this.stepOrder = stepOrder;
	}

	public Date getStepCreated() {
		return stepCreated;
	}

	public void setStepCreated(Date stepCreated) {
		this.stepCreated = stepCreated;
	}

}
