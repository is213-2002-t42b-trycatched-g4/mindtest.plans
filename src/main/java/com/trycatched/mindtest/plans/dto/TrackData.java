package com.trycatched.mindtest.plans.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.trycatched.mindtest.plans.entity.Plan;
import com.trycatched.mindtest.plans.entity.Step;

public class TrackData implements Serializable {

	private static final long serialVersionUID = -8147413847703585644L;

	private Long idUser;
	private int idPlan;
	private String planName;
	private int planActive;
	private String planDesc;
	private int planOrder;
	private Date planCreated;
	private List<TrackStep> steps;

	public TrackData(Long idUser, Plan plan) {
		this.idUser = idUser;
		idPlan = plan.getId();
		planName = plan.getName();
		planActive = plan.getIsActive();
		planDesc = plan.getDescription();
		planOrder = plan.getOrder();
		planCreated = plan.getCreatedAt();

		steps = new ArrayList<>();
		for (Step s : plan.getSteps()) {

			TrackStep ts = new TrackStep();
			ts.setIdStep(s.getId());
			ts.setStepDesc(s.getDescription());
			ts.setStepActive(s.getIsActive());
			ts.setStepOrder(s.getOrder());
			ts.setStepCreated(s.getCreatedAt());

			steps.add(ts);
		}

	}

	public Long getIdUser() {
		return idUser;
	}

	public void setIdUser(Long idUser) {
		this.idUser = idUser;
	}

	public int getIdPlan() {
		return idPlan;
	}

	public void setIdPlan(int idPlan) {
		this.idPlan = idPlan;
	}

	public String getPlanName() {
		return planName;
	}

	public void setPlanName(String planName) {
		this.planName = planName;
	}

	public int getPlanActive() {
		return planActive;
	}

	public void setPlanActive(int planActive) {
		this.planActive = planActive;
	}

	public String getPlanDesc() {
		return planDesc;
	}

	public void setPlanDesc(String planDesc) {
		this.planDesc = planDesc;
	}

	public int getPlanOrder() {
		return planOrder;
	}

	public void setPlanOrder(int planOrder) {
		this.planOrder = planOrder;
	}

	public Date getPlanCreated() {
		return planCreated;
	}

	public void setPlanCreated(Date planCreated) {
		this.planCreated = planCreated;
	}

	public List<TrackStep> getSteps() {
		return steps;
	}

	public void setSteps(List<TrackStep> steps) {
		this.steps = steps;
	}

}
