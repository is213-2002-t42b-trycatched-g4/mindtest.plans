package com.trycatched.mindtest.plans.dto;

public class RequestPlan {

	private Long idUser;

	private int idPlan;

	public RequestPlan(Long idUser, int idPlan) {
		super();
		this.idUser = idUser;
		this.idPlan = idPlan;
	}

	public Long getIdUser() {
		return idUser;
	}

	public int getIdPlan() {
		return idPlan;
	}

}
