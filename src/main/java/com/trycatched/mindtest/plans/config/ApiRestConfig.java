package com.trycatched.mindtest.plans.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "rest.api.client")
public class ApiRestConfig {

	private String urlCheckToken;
	private String urlExistsUser;

	public String getUrlCheckToken() {
		return urlCheckToken;
	}

	public void setUrlCheckToken(String urlCheckToken) {
		this.urlCheckToken = urlCheckToken;
	}

	public String getUrlExistsUser() {
		return urlExistsUser;
	}

	public void setUrlExistsUser(String urlExistsUser) {
		this.urlExistsUser = urlExistsUser;
	}



}
