package com.trycatched.mindtest.plans.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.trycatched.mindtest.plans.service.PlanService;

import util.dto.ApiException;
import util.dto.ApiResponse;
import util.dto.ErrorResponse;

@RestController
@RequestMapping("/api/core")
public class PlanRestController {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	private PlanService planService;

	@Autowired
	public PlanRestController(PlanService planService) {
		this.planService = planService;
	}

	@GetMapping("/plans")
	public ResponseEntity<?> obtainsPlans(@RequestHeader("Authorization") String authorization) {
		ApiResponse response;
		try {
			response = planService.findAll(authorization);
		} catch (ApiException e) {
			logger.error(e.getMessage(), e);
			return new ResponseEntity<>(ErrorResponse.of(e.getCode(), e.getMessage(), e.getDetailMessage()), HttpStatus.PRECONDITION_FAILED);
		}
		return ResponseEntity.ok(response);
	}

}
