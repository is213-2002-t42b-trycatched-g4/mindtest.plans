package com.trycatched.mindtest.plans.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.trycatched.mindtest.plans.business.PlanBusinessService;
import com.trycatched.mindtest.plans.dto.RequestPlan;

import util.dto.ApiException;
import util.dto.ApiResponse;
import util.dto.ErrorResponse;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/core")
public class PlanBusinessRestController {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	private PlanBusinessService planBusinessService;

	@Autowired
	public PlanBusinessRestController(PlanBusinessService planBusinessService) {
		this.planBusinessService = planBusinessService;
	}


	@PostMapping("/purchase-plan")
	public ResponseEntity<?> purchasePlan(@RequestHeader("Authorization") String authorization, @RequestBody RequestPlan requestPlan) {
		ApiResponse response;

		try {
			response = planBusinessService.purchasePlan(authorization, requestPlan);
		} catch (ApiException e) {
			logger.error(e.getMessage(), e);
			return new ResponseEntity<>(ErrorResponse.of(e.getCode(), e.getMessage(), e.getDetailMessage()), HttpStatus.PRECONDITION_FAILED);
		}

		return ResponseEntity.ok(response);
	}

}
