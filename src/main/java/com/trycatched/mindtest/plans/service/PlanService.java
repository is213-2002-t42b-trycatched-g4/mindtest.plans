package com.trycatched.mindtest.plans.service;

import util.dto.ApiException;
import util.dto.ApiResponse;

public interface PlanService {

	public ApiResponse findAll(String authorization) throws ApiException;

}
