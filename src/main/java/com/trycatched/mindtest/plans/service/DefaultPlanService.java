package com.trycatched.mindtest.plans.service;

import java.io.IOException;
import java.util.List;

import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.trycatched.mindtest.plans.client.ApiClientAuthorization;
import com.trycatched.mindtest.plans.config.ApiRestConfig;
import com.trycatched.mindtest.plans.entity.Plan;
import com.trycatched.mindtest.plans.repository.PlanRepository;

import util.constant.ApiError;
import util.dto.ApiException;
import util.dto.ApiResponse;

@Service
public class DefaultPlanService implements PlanService {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	private PlanRepository planRepository;

	private ApiRestConfig restConfig;

	private ApiClientAuthorization apiClientAuth;

	private static final String BEARER = "Bearer ";

	@Autowired
	public DefaultPlanService(PlanRepository planRepository, ApiClientAuthorization apiClientAuth, ApiRestConfig restConfig) {
		this.planRepository = planRepository;
		this.apiClientAuth = apiClientAuth;
		this.restConfig = restConfig;
	}

	@Override
	public ApiResponse findAll(String authorization) throws ApiException {
		ApiResponse authResponse = consultAuthorization(authorization);

		if (isUnauthorized(authResponse)) {
			throw ApiException.of(ApiError.USER_UNAUTHORIZAED.getCode(), ApiError.USER_UNAUTHORIZAED.getMessage());
		}

		List<Plan> plans = planRepository.findAll();
		return ApiResponse.of(ApiError.SUCCESS.getCode(), ApiError.SUCCESS.getMessage(), plans, plans.size());
	}

	private ApiResponse consultAuthorization(String authorization) throws ApiException {
		String token = extractToken(authorization);
		logger.info("token: {}", token);

		CloseableHttpClient httpclient = HttpClients.createDefault();
		ApiResponse authResponse;
		try {
			authResponse = apiClientAuth.verifyAuthorization(httpclient, restConfig.getUrlCheckToken(), token);
		} catch (IOException e) {
			throw ApiException.of(ApiError.NO_APPLICATION_PROCESSED.getCode(), ApiError.NO_APPLICATION_PROCESSED.getMessage(), e.getMessage());
		}
		return authResponse;
	}

	private boolean isUnauthorized(ApiResponse authResponse) throws ApiException {
		JsonNode root = null;
		String message = authResponse.getMessage();
		try {
			root = new ObjectMapper().readTree(message);
			logger.info("result capturado: {}", root);
		} catch (Exception e) {
			throw ApiException.of(ApiError.NO_APPLICATION_PROCESSED.getCode(), ApiError.NO_APPLICATION_PROCESSED.getMessage(), e.getMessage());
		}
		return !root.path("object").asBoolean();
	}

	private String extractToken(String authorization) {
		return authorization.substring(BEARER.length(), authorization.length());
	}

}
