package com.trycatched.mindtest.plans.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "erp_plans_steps")
public class Step implements Serializable {

	private static final long serialVersionUID = -2108195452946040015L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "step_id")
	private Integer id;

	// @Column(name = "plan_id")
	// private Plan plan;

	@Column(name = "step_desc")
	private String description;

	@Column(name = "step_active")
	private int isActive;

	@Column(name = "step_order")
	private int order;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "step_created_at")
	private Date createdAt;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getIsActive() {
		return isActive;
	}

	public void setIsActive(int isActive) {
		this.isActive = isActive;
	}

	public int getOrder() {
		return order;
	}

	public void setOrder(int order) {
		this.order = order;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	@Override
	public String toString() {
		return "Step [id=" + id + ", description=" + description + ", isActive=" + isActive + ", order=" + order
				+ ", createdAt=" + createdAt + "]";
	}

}
