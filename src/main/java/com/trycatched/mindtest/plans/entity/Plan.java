package com.trycatched.mindtest.plans.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;

@Entity
@Table(name = "erp_plans")
public class Plan implements Serializable {

	private static final long serialVersionUID = -7809782955425587715L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "plan_id")
	private Integer id;

	@Size(max = 100)
	@Column(name = "plan_name")
	private String name;

	@Column(name = "plan_active")
	private int isActive;

	@Column(name = "plan_desc")
	private String description;

	@Column(name = "plan_order")
	private Integer order;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "plan_created_at")
	private Date createdAt;

	@OneToMany(fetch = FetchType.LAZY) // carga los steps según lo demande la aplicación
	@JoinColumn(name = "plan_id") // el atributo name especifica el nombre del campo referido en la tabla de unión
	// (es decir erp_plans_steps)
	private List<Step> steps;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getIsActive() {
		return isActive;
	}

	public void setIsActive(int isActive) {
		this.isActive = isActive;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getOrder() {
		return order;
	}

	public void setOrder(Integer order) {
		this.order = order;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public List<Step> getSteps() {
		return steps;
	}

	public void setSteps(List<Step> steps) {
		this.steps = steps;
	}

	@Override
	public String toString() {
		return "Plan [id=" + id + ", name=" + name + ", isActive=" + isActive + ", description=" + description
				+ ", order=" + order + ", createdAt=" + createdAt + ", steps=" + steps + "]";
	}

}
