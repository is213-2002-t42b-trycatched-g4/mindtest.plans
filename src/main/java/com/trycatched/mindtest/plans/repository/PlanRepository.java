package com.trycatched.mindtest.plans.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.trycatched.mindtest.plans.entity.Plan;

@Repository
public interface PlanRepository extends JpaRepository<Plan, Integer>{

}
